# Vaccines

## [Six Month Safety and Efficacy of the BNT162b2 mRNA COVID-19 Vaccine](Six%20Month%20Safety%20and%20Efficacy%20of%20the%20BNT162b2%20mRNA%20COVID-19%20Vaccine.pdf)
### 28/07/2021
### Description
A six-month followup study on Pfizer's mRNA COVID-19 vaccine. "Highly efficacious against COVID-19" according to the article.
### Additional Notes
The study's claim to high efficacy comes from a measure of "cases" in vaccine recipients vs. placebo recipients, based on an N-binding antibody test.
However, the study also shows that the vaccine results in approximately twice as many adverse events as the placebo, with 1.2% of all vaccine recipients experiencing Severe Adverse Events.
Furthermore, the [Supplementary Appendix](Six%20Month%20Safety%20and%20Efficacy%20of%20the%20BNT162b2%20mRNA%20COVID-19%20Vaccine%20(Supplementary%20Appendix).pdf) 
shows that while the number of deaths was statistically the same between the two test groups (both from COVID-19 or from other causes), the vaccinated group saw four deaths from cardiac arrest where the placebo group saw only one.
