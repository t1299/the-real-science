# The Real Science

This repository contains articles and journals related to COVID-19, including the science behind the virus as well as science relating to the various measures governments have implemented in the name of stopping its spread.

# Topic quick links

### [Masks](Masks/maskindex.md)
### [Vaccines](Vaccines/vaccineindex.md)
### [Lockdown](Lockdowns/lockdownindex.md)
### [Misc.](Misc/miscindex.md)

# Format (For contributors)

The repository will be split into folders dedicated to each topic. Each folder will contain all the articles and journals related to its topic, as well as a readme that indexes them and provides a brief description of its contents.

Illustrated:

- Repository root folder
    - Topic 1
        - Index for Topic 1
        - Article 1
        - Article 2
        - ...
    - Topic 2
    - ...


## Index Entries
An index entry should be formatted as follows:

```markdown
## [title of the article](link to the file in the repository)
### date published (DD/MM/YYYY)
### Description
brief description of the article and its findings
### Additional Notes
additional notes about the article (eg. point out flaws in the study, explain if/how it goes agains the narrative)
```
>>>
## [Title of the article](README.md)
### 06/09/2021
### Description
Brief description of the article and its findings
### Additional Notes
Additional notes about the article (eg. point out flaws in the study, explain if/how it goes against the narrative)
>>>
